%skeleton "lalr1.cc"
%require "2.5"
%defines
%define api.namespace {calc}
%define api.value.type variant
%define parser_class_name {Parser}

%code requires {
    #include <iostream>
    #include <memory>
    #include "ast.h"
    namespace calc {class Lexer;}
}

%parse-param {calc::Lexer& lexer} {std::shared_ptr<Ast>& result} {std::string& message}

%code {
    #include "lexer.h"
    #define yylex lexer.lex
}

%token END 0 "end of file"
%token ERROR
%token EOL "\n"

%token <int> NUM

%token PLUS "+"
%token MINUS "-"
%token MUL "*"
%token DIV "/"
%token LPAR "("
%token RPAR ")"

%type <std::shared_ptr<Ast>> expr

%left "+" "-"
%left "*" "/"
%nonassoc UMINUS

%%

input: expr "\n" { result = $1; }

expr: NUM { $$ = new_number($1); }
    | expr "+" expr { $$ = new_binary(Opcode::plus, $1, $3); }
    | expr "-" expr { $$ = new_binary(Opcode::minus, $1, $3); }
    | expr "*" expr { $$ = new_binary(Opcode::mul, $1, $3); }
    | expr "/" expr { $$ = new_binary(Opcode::div, $1, $3); }
    | "(" expr ")" { $$ = $2; }
    | "-" %prec UMINUS expr { $$ = new_unary(Opcode::uminus, $2); }
;
%%

void calc::Parser::error(const std::string& err)
{
    message = err;
}
