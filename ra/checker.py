#!/usr/bin/python3
# -*- coding: utf-8 -*-

import subprocess
import argparse
import annotated
from collections import OrderedDict

RA_JAR="ra.jar"
RA_PROPERTIES="pizza.prop"

class Ra:
    BREAKER = "-----"

    def __init__(self, ra=RA_JAR, prop=RA_PROPERTIES):
        self._ra = ra;
        self._prop = prop

    def _exec(self, statement):
        statement += ";\n"
        raw_output = subprocess.check_output(["java", "-jar", self._ra, self._prop], input=statement.encode('utf-8'))
        return raw_output

    def _extract_answer(self, raw_output):
        lines = raw_output.split(b"\n")
        breaks = [i for i,line in enumerate(lines) if line == self.BREAKER.encode('utf-8')]
        assert len(breaks) == 2
        lines = lines[breaks[0] + 1 : breaks[1]]
        return [l.decode("utf-8") for l in lines]

    def _validate(self, statement):
        assert len(statement) > 0
        assert "\sqlexec" not in statement
        assert ";" not in statement
        assert "\n" not in statement
        assert self.BREAKER not in statement

    def run(self, statement):
        if len(statement) > 0 and statement[-1] == ';':
            statement = statement[:-1]
        self._validate(statement)
        raw_output = self._exec(statement)
        return self._extract_answer(raw_output)


class Main:
    def __init__(self, ra_jar, ra_prop, queries_filename, answers_filename, use_json):
        self._ra = Ra(ra_jar, ra_prop)
        self._queries_filename = queries_filename
        self._answers_filename = answers_filename
        self._user_json = use_json

    def _run_queries(self, queries):
        return OrderedDict((test, self._ra.run(query)) for test, query in queries.items())

    def _validate_results(self, query_results, correct_answers):
        for test in correct_answers.keys():
            if test not in query_results.keys():
                raise Exception("No resutl for test \"{0}\"".format(test))
            result = query_results[test]
            answer = correct_answers[test]
            result = "\n".join(sorted(result))
            answer = "\n".join(sorted(answer))
            if result != answer:
                raise Exception("Result for test \"{0}\" is incorrect".format(test))
            print("Test \"{0}\" passed!".format(test))
        print("Everything OK")

    def _load_answers(self):
        if not self._user_json:
            answers = annotated.load_text(self._answers_filename)
            return OrderedDict((test, lines.split("\n")) for test, lines in answers.items())
        else:
            return annotated.load_json(self._answers_filename)

    def _dump_answers(self, answers):
        if not self._user_json:
            answers = OrderedDict((test, "\n".join(lines)) for test, lines in answers.items())
            annotated.dump_text(self._answers_filename, answers)
        else:
            annotated.dump_json(self._answers_filename, answers)

    def validate_queries(self):
        queries = annotated.load_text(self._queries_filename)
        answers = self._load_answers()
        results = self._run_queries(queries)
        self._validate_results(results, answers)

    def generate_answers(self):
        queries = annotated.load_text(self._queries_filename)
        answers = self._run_queries(queries)
        self._dump_answers(answers)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--gen", action="store_true", default=False, help="Generate answers from queries")
    parser.add_argument("--json", action="store_true", default=False, help="Print answers in json format")
    parser.add_argument("--ra", type=str, default=RA_JAR, help="Path to relational algebra interpreter jar executable")
    parser.add_argument("--prop", type=str, default=RA_PROPERTIES, help="Path to relational algebra interpreter properties")
    parser.add_argument("queries", type=str, help="File with queries")
    parser.add_argument("answers", type=str, help="File with answers")
    args = parser.parse_args()

    main = Main(args.ra, args.prop, args.queries, args.answers, args.json)
    if args.gen:
        main.generate_answers()
    else:
        main.validate_queries()

